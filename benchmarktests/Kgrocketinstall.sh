#!/bin/bash
module purge
# load a recent gcc compiler suite
module load openmpi-1.7.3

# go to home directory
cd $HOME
# create directory for code
export KG=kg
mkdir $KG
cd $KG
wget kodu.ut.ee/~benson/arXiv-1501-04552v1.tar.gz
tar -xvf arXiv-1501-04552v1.tar.gz
cd anc/
cd 2decomp_fft/
cp src/Makefile.inc.x86 src/Makefile.inc
make
cd ..
cd code/
export HOST=Rocket
cp Makefile.example makefile
# change compilation options for Rocket to use openmpi rather
# than Intel MPI
sed '253 c COMPILER = mpif90' makefile > makefile2
sed '257 c        LIBS= ' makefile2 > makefile
sed '254 c CPP = -cpp' makefile > makefile2
sed '256 c FLAGS = -O3' makefile2 > makefile
rm makefile2
# Compile code
make
# create submission script 
touch subscript
echo '#!/bin/bash ' >> subscript              
echo '#SBATCH -N 4 ' >> subscript             
echo '#SBATCH --ntasks-per-node=20 ' >> subscript
echo '#SBATCH --mem=20000' >> subscript
echo 'module load openmpi-1.7.3' >> subscript
echo ' ' >>  subscript
echo '# indicate where to get problem dependent parameters' >> subscript
echo 'export inputfile=INPUTFILE' >> subscript
echo '# run code' >> subscript
echo 'mpirun Kg' >> subscript
# run code by submitting script
sbatch subscript